import { Controller } from 'egg';
import * as debug from 'debug';

export default class MailController extends Controller {
  private mailControllerDebug: Function;

  constructor(ctx) {
    super(ctx);
    this.mailControllerDebug = debug('app:controller:mail');
  }

  public async index() {
    const { ctx } = this;
    ctx.validateBySchema(ctx.app.config.mailJsonSchema);
    try {
      await ctx.service.mail.send(ctx.request.body);
      this.mailControllerDebug('Email send succeed!');
    } catch (err) {
      this.ctx.throw(503, 'Email Send Failed', {
        code: err.code,
        errors: err.message,
      });
    }

    ctx.body = 'send email succeed.';
  }
}
