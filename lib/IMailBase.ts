import { IMessage } from "./index";

export interface IMailBase {
  name: string;

  /**
   * send email
   * @param message - mail message body
   */
  send(message: IMessage);

  /**
   * check Health
   */
  checkHealth();
}
