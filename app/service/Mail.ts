import { Application, Service } from 'egg';
import { MailCluster, IMessage } from "../../lib/index";

export interface AppConfig {
  mailCluster: MailCluster;
}

export default (app: Application & AppConfig) => {
  const mailCluster: MailCluster = new MailCluster(app);
  app.mailCluster = mailCluster;

  /**
   * Mail Service
   */
  class MailService extends Service {
    /**
     * send email
     * @param message - mail message body
     */
    public async send(message: IMessage) {
      await mailCluster.send(message);
    }
  }

  return MailService;
};
