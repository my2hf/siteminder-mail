import { IMessageContent, IMessageEmail } from "../IMessage";

interface IOption {
  enable: boolean;
}

interface IMailSetting {
  sandbox_mode: IOption;
}

export interface IPersonalization {
  to: Array<IMessageEmail>;
  cc?: Array<IMessageEmail>;
  bcc?: Array<IMessageEmail>;
  subject?: string;
}

export interface ISendGridMessage {
  personalizations: Array<IPersonalization>;
  from: IMessageEmail;
  subject: string;
  content: Array<IMessageContent>;
  mail_settings?: IMailSetting;
}
