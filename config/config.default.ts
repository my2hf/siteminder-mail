import { EggAppConfig, PowerPartial } from 'egg';
import * as mailSchema from './mailSchema.json';
import * as ms from 'ms';
import * as path from 'path';

export interface ProviderConfig {
  API_KEY: string;
  apiUrl: string;
  checkHealthUrl: string;
}

export interface BizConfig {
  mailgun: ProviderConfig;
  sendgrid: ProviderConfig;
  checkInterval: number;
}

// for config.{env}.ts
export type DefaultConfig = PowerPartial<EggAppConfig & BizConfig>;

export default (appInfo: EggAppConfig) => {
  const config = {} as PowerPartial<EggAppConfig> & BizConfig;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1524972496894_5478';

  // add your config here
  config.middleware = ['errorHandler'];

  config.mailJsonSchema = mailSchema;

  config.mailgun = {
    API_KEY: process.env.MAILGUN_API_KEY,
    apiUrl: `https://api.mailgun.net/v3/${process.env.MAILGUN_DOMAIN}/messages`,
    checkHealthUrl: `https://api.mailgun.net/v3/${process.env.MAILGUN_DOMAIN}/stats/total`
  };

  config.sendgrid = {
    API_KEY: process.env.SENDGRID_API_KEY,
    apiUrl: 'https://api.sendgrid.com/v3/mail/send',
    checkHealthUrl: 'https://api.sendgrid.com/v3/mail/send'
  };

  config.checkInterval = ms('20s');

  config.customLogger = {
    mailProviderLogger: {
      file: path.join(appInfo.root, 'logs/mail/provider.log'),
    },
  };

  config.validateSchema = {
    useDefaults: true,
  };

  return config;
};
