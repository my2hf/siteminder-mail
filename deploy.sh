#!/bin/bash

ACTION=$1
USER_HOME_DIR=$2

usage()
{
	echo ""
	echo "Usage: `basename $0` [options]"
    echo ""
	echo "Options:"
    echo ""
    echo "  build: build the application packge for deployment"
	echo "  start: start server"
    echo "  stop : stop server"
    echo "  start: stop server and then start server"
	echo ""
	exit 0
}

build() {
  npm install
  npm run tsc
  rm -rf /tmp/siteminder-mail.tar.gz
  tar --exclude=typings \
    --exclude=test \
    --exclude=run \
    --exclude=.git \
    --exclude=logs \
    --exclude=coverage \
    -czf /tmp/siteminder-mail.tar.gz ./*
}

start() {
  source "${USER_HOME_DIR}/.setup.sh"
  npm start
}

stop() {
  npm stop
}

case "${ACTION}" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        stop
        start
    ;;
    build)
        build
    ;;
    *)
        usage
    ;;
esac
