import { Application } from 'egg';

import { IMessage, IMailBase, ErrorBase } from "../index";
import { IMailGunMessage } from "./IMailGunMessage";

class MailGun implements IMailBase {
  private _app: Application;
  private _apiUrl: string;
  private _checkHealthUrl: string;
  private _authorization: string;

  public name: string = 'MailGunMailProvider';

  constructor(app: Application) {
    this._app = app;
    this._apiUrl = app.config.mailgun.apiUrl;
    this._checkHealthUrl = app.config.mailgun.checkHealthUrl;
    this._authorization = `Basic ${Buffer.from('api:' + app.config.mailgun.API_KEY).toString('base64')}`;
  }

  /**
   * Send email.
   * @param message - mail message body
   */
  private async _send(message: IMessage) {
    const sendMessage: IMailGunMessage = this._constructMessage(message);
    const result = await this._app.curl(this._apiUrl, {
      method: 'POST',
      data: sendMessage,
      headers: {
        Authorization: this._authorization,
      },
      dataType: 'json',
      // 1s timeout of connection creation, and the 5s timeout of receive response for the responsing of larger scenarios
      timeout: [ 1000, 5000 ],
    });
    return result;
  }

  /**
   * Construct the MailGun message body.
   * @param message - mail message body
   */
  private _constructMessage(message: IMessage): IMailGunMessage {
    const tos: Array<string> = message.to.map(m => `${m.name || ''} <${m.email}>`);
    const sendMessage: IMailGunMessage = {
      from: `${message.from.name} <${message.from.email}>`,
      to: tos.join(', '),
      subject: message.subject,
      text: message.content[0].value,
    };

    if (message.cc && message.cc.length > 0) {
      const ccs: Array<string> = message.cc.map(m => `${m.name || ''} <${m.email}>`);
      sendMessage.cc = ccs.join(', ');
    }

    if (message.bcc && message.bcc.length > 0) {
      const bccs: Array<string> = message.bcc.map(m => `${m.name || ''} <${m.email}>`);
      sendMessage.bcc = bccs.join(', ');
    }

    return sendMessage;
  }

  /**
   * Send email. If fail, retry once.
   * @param message - mail message body
   */
  public async send(message: IMessage) {
    let result = await this._send(message);
    if (result.status !== 200) {// retry once
      result = await this._send(message);
    }

    if (result.status !== 200) {// throw error
      const error = new ErrorBase();
      error.name = 'MailgunProviderSendEmailFailed';
      error.message = result.data.message;
      error.code = 'mailgun_fail';
      this._app.getLogger('mailProviderLogger').info(error);
      throw error;
    }
  }

  public async checkHealth() {
    const result = await this._app.curl(this._checkHealthUrl,
      {
        method: 'GET',
        data: {
          event: 'failed',
          duration: '1d',
        },
        headers: {
          Authorization: this._authorization,
        },
        dataType: 'json',
        // 1s timeout of connection creation, and the 5s timeout of receive response for the responsing of larger scenarios
        timeout: [ 1000, 5000 ],
      });

    if (result.status !== 200) {
      const error = new ErrorBase();
      error.message = 'MailGun Provider checkHealth failed!';
      error.code = 'mailgun_checkHealth_fail';
      throw error;
    }

    return result;
  }
}

export { MailGun };
