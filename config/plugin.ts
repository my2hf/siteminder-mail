import { EggPlugin } from 'egg';

const plugin: EggPlugin = {
  static: {
    enable: true,
  },
  security: {
    enable: false,
  },
  validateSchema: {
    enable: true,
    package: 'egg-validate-schema',
  },
};

export default plugin;
