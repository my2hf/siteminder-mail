import { IMailBase } from "./IMailBase";
import { ErrorBase } from "./ErrorBase";
import { MailCluster } from "./MailCluster";
import { IMessage, IMessageEmail, IMessageContent } from "./IMessage";

import { SendGridMail } from "./sendgrid/SendGridMail";
import { MailGun } from "./mailgun/MailGun";

export {
  IMailBase, IMessage, IMessageEmail,
  IMessageContent, ErrorBase,
  SendGridMail, MailGun,
  MailCluster
};
