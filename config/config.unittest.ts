import * as ms from 'ms';
import { DefaultConfig } from './config.default';

export default () => {
  const config: DefaultConfig = {};

  config.checkInterval = ms('2s');

  return config;
};
