import { Application } from 'egg';

import { IMessage, IMailBase, ErrorBase } from "../index";
import { ISendGridMessage } from "./ISendGridMessage";

class SendGridMail implements IMailBase {
  private _app: Application;
  private _apiUrl: string;
  private _checkHealthUrl: string;
  private _SENDGRID_API_KEY: string;

  public name: string = 'SendGridMailProvider';

  constructor(app: Application) {
    this._app = app;
    this._apiUrl = app.config.sendgrid.apiUrl;
    this._checkHealthUrl = app.config.sendgrid.checkHealthUrl;
    this._SENDGRID_API_KEY = app.config.sendgrid.API_KEY;
  }

  /**
   * Send email.
   * @param message - mail message body
   */
  private async _send(message: IMessage) {
    const sendMessage: ISendGridMessage = this._constructMessage(message);
    const result = await this._app.curl(this._apiUrl, {
      method: 'POST',
      contentType: 'json',
      data: sendMessage,
      headers: {
        authorization: `Bearer ${process.env.SENDGRID_API_KEY}`,
      },
      dataType: 'json',
      // 1s timeout of connection creation, and the 5s timeout of receive response for the responsing of larger scenarios
      timeout: [ 1000, 5000 ],
    });
    return result;
  }

  /**
   * Construct the SendGrid message body.
   * @param message - mail message body
   */
  private _constructMessage(message: IMessage): ISendGridMessage {
    return {
      personalizations: [
        {
          to: message.to,
          cc: message.cc,
          bcc: message.bcc,
        }
      ],
      from: message.from,
      subject: message.subject,
      content: message.content,
    };
  }

  /**
   * Send email. If fail, retry once.
   * @param message - mail message body
   */
  public async send(message: IMessage) {
    let result = await this._send(message);
    if (result.status !== 202) {// retry once
      result = await this._send(message);
    }

    if (result.status !== 202) {// throw error
      const error = new ErrorBase();
      error.name = 'SendGridProviderSendEmailFailed!';
      error.message = result.data.errors[0].message;
      error.code = 'sendgrid_fail';
      this._app.getLogger('mailProviderLogger').info(error);
      throw error;
    }
  }

  // https://github.com/sendgrid/sendgrid-csharp/issues/673
  public async checkHealth() {
    const message: ISendGridMessage = {
      personalizations: [
        {
          to: [{
            email: 'checkHealth@example.com'
          }],
        }
      ],
      from: {
        email: 'checkHealth@example.com'
      },
      subject: 'check',
      content: [
        {
          "type": "text",
          "value": "checkHealth"
        }
      ],
      mail_settings: {
        sandbox_mode: {
          enable: true,
        }
      }
    };

    const result = await this._app.curl(this._checkHealthUrl, {
      method: 'POST',
      contentType: 'json',
      data: message,
      headers: {
        authorization: `Bearer ${this._SENDGRID_API_KEY}`,
      },
      dataType: 'json',
      // 1s timeout of connection creation, and the 5s timeout of receive response for the responsing of larger scenarios
      timeout: [ 1000, 5000 ],
    });

    if (result.status !== 200) {
      const error = new ErrorBase();
      error.message = 'SendGrid Provider checkHealth failed!';
      error.code = 'sendgrid_checkHealth_fail';
      throw error;
    }
  }
}

export { SendGridMail };
