import * as sdkBase from 'sdk-base';
import * as interval from 'interval-promise';
import * as debug from 'debug';
import { Application } from 'egg';

import { IMailBase, SendGridMail, MailGun, IMessage, ErrorBase } from "./";

export class MailCluster extends sdkBase {
  private _servers: Array<IMailBase> = [];
  private _availables: Array<boolean> = [];
  private _app: Application;
  private _checkAvailableLock: boolean = false;
  private _mailClusterDebug = debug('mail-service:lib:MailCluster.ts');

  constructor(app: Application) {
    super();
    this._app = app;
    this._servers.push(new SendGridMail(this._app));
    this._servers.push(new MailGun(this._app));

    for (let i = 0; i < this._servers.length; i++) {
      this._availables[i] = true;
    }

    const _checkAvailable = this.checkAvailable.bind(this);
    interval(_checkAvailable, app.config.checkInterval);
  }

  public async checkAvailable() {
    if (this._checkAvailableLock) return;
    this._checkAvailableLock = true;

    const downServers: Array<IMailBase> = [];
    for (let i = 0; i < this._servers.length; i++) {
      const server: IMailBase = this._servers[i];
      // check 3 times
      let available = await this._checkStatus(server);
      if (!available) {
        // check again
        available = await this._checkStatus(server);
      }
      if (!available) {
        // check again
        available = await this._checkStatus(server);
        if (!available) {
          downServers.push(server);
        }
      }
      this._availables[i] = available;
    }

    if (downServers.length) {
      const error = new ErrorBase();
      error.message = 'mail cluster server down error';
      error.data = this._formatServerNames(downServers);
      error.name = 'MailClusterAvailableError';
      this._app.emit('error', error); // The Egg.js will log the error to Error logfiles.
    }

    const availableServers: Array<IMailBase> = [];
    for (let i = 0; i < this._servers.length; i++) {
      if (this._availables[i]) {
        availableServers.push(this._servers[i]);
      }
    }

    this._mailClusterDebug('_checkAvailable:', this._formatServerNames(availableServers));

    this._checkAvailableLock = false;
  }

  private _formatServerNames(servers: Array<IMailBase>): string {
    let names: string = '[ ';
    names += servers.reduce((a: string, b: IMailBase) => a += `${a ? ',' : ''}'${b.name}'`, '');
    names += ' ]';
    return names;
  }

  /**
   * checkStatus
   * @param server - email provider server instance.
   */
  private async _checkStatus(server: IMailBase) {
    let available = true;
    try {
      await server.checkHealth();
    } catch (err) {
      this._mailClusterDebug('_checkStatus:', err.message);
      available = false;
    }
    return available;
  }

  /**
   * choseAvailable
   */
  private _choseAvailable(): IMailBase {
    for (let i = 0; i < this._servers.length; i++) {
      if (this._availables[i]) {
        return this._servers[i];
      }
    }

    // all down, try to use this first one
    return this._servers[0];
  }

  /**
   * send email
   * @param message - mail message body
   */
  public async send(message: IMessage) {
    const client: IMailBase = this._choseAvailable();
    return await client.send(message);
  }
}
