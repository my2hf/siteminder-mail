export class ErrorBase extends Error {
  public code: string;
  public message: string;
  public data: string;
}
