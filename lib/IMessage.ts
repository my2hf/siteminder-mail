export interface IMessageEmail {
  name?: string;
  email: string;
}

export interface IMessageContent {
  type: string;
  value: string;
}

export interface IMessage {
  from: IMessageEmail;
  subject: string;
  content: Array<IMessageContent>;
  to: Array<IMessageEmail>;
  cc?: Array<IMessageEmail>;
  bcc?: Array<IMessageEmail>;
}
