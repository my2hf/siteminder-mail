# ARCHITECTURE

## Architectural Choice

- Platform: [Node.js](https://nodejs.org/) as the requirement.
- Language: [TypeScript](https://www.typescriptlang.org/), which is easy to maintian reduce bugs.
- Input Validation: [Ajv: Another JSON Schema Validator](https://github.com/epoberezkin/ajv), which is a powerful and esay to use tool for input validation.
- Web Framework: [Egg.js](https://eggjs.org/), a enterpirce level web framework which a esay to use and ready for production.
- Httpclient: [urllib](https://github.com/node-modules/urllib), a simple httpclient integrated with Egg.js.

## Constraints

- Email Service Provider should be less than 10 in the predictable future.
- This server must support horizontal scale.
- Suppporting auto switch when particular provider down.
- Support malformed input validation.
- No authecaiton required.

## Error/Data Loss/Slow Response

- When the task failed , we could retry at once. It will improve the success rate by this method.
- Keep the health check in the backgroup, auto switch the provider.
- When any provider down, log the error, and make sure there is minitor system can alert us in time.
- Set the request timeout to handle the slow reponse situation. For example, 1s for connection, 5s for receive the response.

## Auditing Capabilities

- Access Log: Log the detail of every request. We can caculate the QPS(Query Per Second) of the server based on this.
- Record all success send email transactions, So that we will know the the success rate.
- Log all the provider down errors, so we will know what is the status of the providers.
- Some basic data， such as CPU utilization, memory usage, disk usage, traffic and so on.
