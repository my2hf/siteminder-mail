import * as assert from 'assert';
import { app, mock } from 'egg-mock/bootstrap';
import { IMessage } from "../../../lib/index";

function sleep(timeout) {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

describe('test/app/controller/mail.test.ts', () => {
  before(() => { });
  afterEach(mock.restore);

  const messageBody: IMessage = {
    "from": {
      "name": "hello",
      "email": "mail@hello.world"
    },
    "subject": "Test Mail",
    "content": [
      {
        "type": "text/plain",
        "value": "This is an test email."
      }
    ],
    "to": [
      {
        "name": "some one",
        "email": "459051832@qq.com"
      }
    ]
  };

  it('should SEND email ok', async () => {
    const result = await app.httpRequest()
      .post('/mail/message')
      .send(messageBody)
      .expect(200);

    assert(result.text === 'send email succeed.');
  });

  it('should malformed IMessage throw', async () => {
    mock(messageBody, 'from', '');
    const result = await app.httpRequest()
      .post('/mail/message')
      .send(messageBody)
      .expect(422);

    assert(result.body.error === 'Validation Failed');
  });

  it('should throw when all Mail Providers down', async () => {
    app.mockHttpclient(app.config.sendgrid.apiUrl, 'POST', {
      status: 503,
      data: {},
    });
    app.mockHttpclient(app.config.sendgrid.checkHealthUrl, 'POST', {
      status: 503,
      data: {},
    });

    app.mockHttpclient(app.config.mailgun.apiUrl, 'POST', {
      status: 503,
      data: {},
    });
    app.mockHttpclient(app.config.mailgun.checkHealthUrl, 'GET', {
      status: 503,
      data: {},
    });

    await sleep(4000);

    const result = await app.httpRequest()
      .post('/mail/message')
      .send(messageBody)
      .expect(503);

    assert(result.body.error === 'Email Send Failed');
  });

  it('should throw when first Providers down and the second down', async () => {
    app.mockHttpclient(app.config.sendgrid.apiUrl, 'POST', {
      status: 503,
      data: {},
    });
    app.mockHttpclient(app.config.sendgrid.checkHealthUrl, 'POST', {
      status: 503,
      data: {},
    });

    await sleep(4000);

    app.mockHttpclient(app.config.mailgun.apiUrl, 'POST', {
      status: 503,
      data: {},
    });

    const result = await app.httpRequest()
      .post('/mail/message')
      .send(messageBody)
      .expect(503);

    assert(result.body.error === 'Email Send Failed');
  });

  it('should checkAvailable work ok', async () => {
    let tag = 0;
    app.on('error', () => tag++);
    await app.mailCluster.checkAvailable();
    await sleep(5000);
    assert(tag === 0);
  });

  it('should checkAvailable throw', async () => {
    app.mockHttpclient(app.config.mailgun.checkHealthUrl, 'GET', {
      status: 503,
      data: {},
    });

    app.mockHttpclient(app.config.sendgrid.checkHealthUrl, 'POST', {
      status: 503,
      data: {},
    });

    await new Promise(function (resolve, reject) {
      app.on('error', err => {
        try {
          assert(err.message === 'mail cluster server down error');
          assert(err.data === `[ 'SendGridMailProvider','MailGunMailProvider' ]`);
          assert(err.name === 'MailClusterAvailableError');
          resolve();
        } catch (e) {
          reject(e);
        }
      });
    });
  });

  it('should auto switch', async () => {
    await app.mailCluster.checkAvailable();
    app.mockHttpclient(app.config.sendgrid.apiUrl, 'POST', {
      status: 503,
      data: {},
    });
    app.mockHttpclient(app.config.sendgrid.checkHealthUrl, 'POST', {
      status: 503,
      data: {},
    });

    await new Promise(function (resolve, reject) {
      app.on('error', err => {
        try {
          assert(err.message === 'mail cluster server down error');
          assert(err.data === `[ 'SendGridMailProvider' ]`);
          assert(err.name === 'MailClusterAvailableError');
          resolve();
        } catch (e) {
          reject(e);
        }
      });
    });

    mock(messageBody, 'cc', [
      {
        "name": "demo",
        "email": "459051832@qq.com"
      },
    ]);

    mock(messageBody, 'bcc', [
      {
        "name": "demo",
        "email": "459051832@qq.com"
      },
    ]);

    const result = await app.httpRequest()
      .post('/mail/message')
      .send(messageBody)
      .expect(200);

    assert(result.text === 'send email succeed.');
  });
});
