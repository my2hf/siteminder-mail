export interface IMailGunMessage {
  from: string;
  subject: string;
  text: string;
  to: string;
  cc?: string;
  bcc?: string;
}
