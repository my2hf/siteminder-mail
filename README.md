# mail-service

mail service 

### Preparation

This project needs some basic information from mail provider `sendgrid` and `mailgun`.

This information should be export to the system environment variable.

- sendgrid
  - `API_KEY`: ** export SENDGRID_API_KEY='<Your API_KEY>' **
- mailgun
  - `API_KEY`: ** export MAILGUN_API_KEY='<Your API_KEY>' **
  - `DOMAIN`: ** export MAILGUN_DOMAIN='<Your DOMAIN>' **

quick set:

```bash
export SENDGRID_API_KEY='<Your API_KEY>'
export MAILGUN_API_KEY='<Your API_KEY>'
export MAILGUN_DOMAIN='<Your DOMAIN>'
```

### Requirement

- Node.js 8.x
- Typescript 2.8+

## QuickStart

The main feature of this project is to send the email.

Before the start, we need install dependencies.

```bash
npm install
```

After that, we can start the server like this:

```bash
npm run dev
```

After that we will see the log :

```bash
egg started on http://127.0.0.1:7001
```

And then, we can have a try:

```bash
curl -i \
	-d '{"from": {"email": "mail@hello.world"},"subject": "hello","content": [{"value": "world","type": "text/plain"}],"to": [{"email": "ngot@ngot.me"}]}' \
	-H "Content-Type: application/json" \
	-X POST http://localhost:7001/mail/message
```

Don't tsc compile at development mode, if you had run `tsc` then you need to `npm run clean` before `npm run dev`.

### Directory Structure
```
├── app // Application Directory
│   ├── controller // Controller Directory
│   │   └── mail.ts
│   ├── middleware // Middleware Directory
│   │   └── error_handler.ts
│   ├── public // Static File Directory
│   │   └── index.html
│   ├── router.ts // Application Router
│   └── service // Service Directory, service can be invoked by controller
│       └── Mail.ts
├── config // Config Directory
│   ├── config.default.ts
│   ├── config.local.ts // development config
│   ├── config.prod.ts // production config
│   ├── config.unittest.ts // unittest config
│   ├── mailSchema.json // input validation config
│   └── plugin.ts // plugin config
├── lib // send email logic 
│   ├── ErrorBase.ts
│   ├── IMailBase.ts // send email interface definition
│   ├── IMessage.ts
│   ├── MailCluster.ts
│   ├── index.ts
│   ├── mailgun // MailGun provider implementation
│   │   ├── IMailGunMessage.ts // mailgun message definition
│   │   └── MailGun.ts // MailGun send email logic implementation
│   └── sendgrid // SendGrid provider implementation
│       ├── ISendGridMessage.ts // SendGrid message definition
│       └── SendGridMail.ts // SendGrid send email logic implementation
├── package.json
├── test
│   └── app
│       └── controller
│           └── mail.test.ts
├── tsconfig.json
└── tslint.json
```

### Test

Run unit test only:

```bash
npm run test-local
```

Run unit test with code lint:

```bash
npm run test
```

Run unit test with coverage and code lint:

```bash
npm run cov
```

### Deploy

#### How to build?

Execute the following command, and then we can find the application package at `/tmp/siteminder-mail.tar.gz`.

#### How to Deploy?

Chose your favorite method to upload the package, and then extract the package.

- SCP
- FTP
- Docker
- Pipeline

#### How to start the server?

At the application root directory, execute the command below.

```bash
$ npm start
```

#### How to stop the server?

At the application root directory, execute the command below.

```bash
$ npm stop
```

### Npm Scripts

- Use `npm run lint` to check code style
- Use `npm test` to run unit test
- Use `npm run clean` to clean compiled js at development mode once
